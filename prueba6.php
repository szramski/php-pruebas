<?php

    $texto1 = "Hola";
    $texto2 = "estamos aprendiendo PHP";

    echo $texto1 . " " . $texto2 . "<br><br><hr>";

    /*********************/

    $n1 = 10;
    $n2 = 5;

    echo "La suma de " 
            . $n1
            . " y " 
            . $n2 
            . " es: " 
            . ($n1 + $n2);

    /*********************/

    echo "<br><br><hr>";

    $arr = ["verde", "rojo", "azul"];

    foreach($arr as $color){
        echo $color . " ";
    }//foreach

    echo "<ul>";
    foreach($arr as $color){
        echo "<li>" . $color . "</li>";
    }//foreach
    echo "</ul>";
    
    /*********************/
    echo "<br><br><hr>";

    echo "<table>
            <tr>";

    for ($i = 1; $i<=100; $i++ ){

        echo "<td>" . $i . "</td>";

        if ($i % 10 == 0){
            echo "</tr><tr>";
        }//if

    }//for i

    echo "</tr>
        </table>";


    /*********************/
    
    echo "<br><br><hr>";

    $v = [1 => 90, 30 => 7, 'e' => 99, 'hola' => 43];

    echo "<ul>";
    foreach($v as $key => $value){

        if( gettype($key) == 'string'){
            echo '<li>$v[\'' .$key . '\'] = ' . $value . '</li>';
        } else {
            echo '<li>$v[' . $key . '] = ' . $value . '</li>';
        }//else        
    }//foreach
    echo "</ul>";


    /*********************/
    echo "<br><br><hr>";

    $arr2 = ["red", "green", "blue", "yello", "black", "white"];

    print_r($arr2);

    unset($arr2[2]);
    unset($arr2[4]);

    echo "<br><br>";
    print_r($arr2);

    $arr2 = array_values($arr2);

    echo "<br><br>";
    print_r($arr2);


    /*********************/

    echo "<br><br><hr>";

    $n = 5;

    for($i=1; $i <= $n; $i++){
        
        for($j = 1; $j <= $i; $j++){
            echo "* ";
        }//for j
        echo "<br>";
    }//for i

    for($i=$n; $i >= 1; $i--){
        
        for($j = 1; $j <= $i; $j++){
            echo "* ";
        }//for j
        echo "<br>";
    }//for i

    /*********************/

    echo "<br><br><hr>";

    function imprimaFactorial($n){

        if($n <= 0){
            return $n . ": Este no es un valor válido para
                            calcular su factorial";
        } else {
            $texto = $n . ": ";

            $producto = 1;

            for($i = $n; $i >= 1; $i--){

                $producto = $producto * $i;

                if($i != 1){
                    $texto = $texto . $i . " * ";
                } else {
                    $texto = $texto . $i . " = " . $producto;   
                }//if
                
            }//for

            return $texto;
        }//else

    }//function

    echo imprimaFactorial(5);

    /*********************/

    echo "<br><br><hr>";

    echo "<style>
    
    *{
        padding: 0;
        margin:0;
        box-sizing: border-box;
    }

    .container{
        width: 160px; 
        
    }
    
    .box{width: 20px; height: 20px; float: left}

    .black{background-color: black};
    .white{background-color: white};
   
    </style>";

    /*
    echo '<div class="container">';

    $color = "black";

    for($i = 1; $i<=64; $i++){

        if ($color == "black"){
            echo '<div class="box black"></div>';
                
            $color = "white";
        
        } else {
            echo '<div class="box white"></div>';
            $color = "black";
        }//if       
    }//for
       
    echo '</div>';

    

    echo "<br><br><hr>";
*/

    $str = "Hola Mundo";
    $new = [];

    for($i = (strlen($str) -1); $i >=0 ; $i--){

        array_push( $new, $str[$i] );        

    }//for

    $strInv = implode("", $new);

    echo $strInv;
?>