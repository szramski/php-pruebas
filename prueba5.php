<?php

    echo "pow: " .   pow(4,2)     . "<br>";
    echo "rand: " .  rand(1,1000) . "<br>";
    echo "sqrt of 4: " .  sqrt(4)      . "<br>";
    echo "ceil of 4.6: " .  ceil(4.6)    . "<br>";
    echo "ceil of 4.3: " .  ceil(4.3)    . "<br>";
    echo "floor of 4.6: " . floor(4.6)   . "<br>";
    echo "round of 4.6: " . round(4.6)   . "<br>";
    echo "round of 4.3: " . round(4.3)   . "<br>";
?>